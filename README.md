# Deploy
- `oc new-app https:/this_repo`
- `oc expose svc/isso`
- Then on Openshift web portal, go to Applications menu > routes > click on the route > edit > set secure > Edge and Redirect insecure traffic.