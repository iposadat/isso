FROM alpine:3.5
MAINTAINER web-services-core, <web-services-core@cern.ch>

RUN apk upgrade --update \
 && apk add python3 python3-dev sqlite gcc musl-dev ca-certificates openssl \
 && update-ca-certificates \
 && pip3 install isso gunicorn gevent \
 && mkdir -p /opt/isso \ 
 && mkdir -p /tmp/issodb \
 && chmod -R g+w /tmp/issodb

COPY isso.conf /opt/isso/

EXPOSE 8080

CMD /usr/bin/gunicorn -k gevent -b 0.0.0.0:8080 -w 4 --preload isso.run